﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Utils.Input;

public class CanvasCreditsPresenter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _titleText,
        _programmersTitle,
        _gameDesignerTitle,
        _artistVFXTitle,
        _compositorsTitle,
        _artist3DTitle,
        _artist2DTitle;

    [SerializeField] private Button _backButton;
    private SceneChanger _sceneChanger;
    private ILanguageManager _languageManager;
    private ReadInputPlayer _readInputPlayer;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();

    }

    void Start()
    {
        _backButton.onClick.AddListener(GoToInitScene);
        SetLanguageStrings(_languageManager.GetActualLanguageText());
        _readInputPlayer.ShowCursor();
        _readInputPlayer.ReleaseCursor();
    }

    private void OnDestroy()
    {
        _backButton.onClick.RemoveListener(GoToInitScene);
    }

    private void GoToInitScene()
    {
        _sceneChanger.GoToInitScene();
    }

    private void SetLanguageStrings(LanguageText languageText)
    {
        _titleText.SetText(languageText.TitleCreditsSceneCreditsKey);
        _programmersTitle.SetText(languageText.ProgrammersTitleCreditsKey);
        _artistVFXTitle.SetText(languageText.ArtistVFXTitleCreditsKey);
        _compositorsTitle.SetText(languageText.CompositorsVFXTitleCreditsKey);
        _artist3DTitle.SetText(languageText.Artist3DTitleCreditsKey);
        _artist2DTitle.SetText(languageText.Artist2DTitleCreditsKey);
        _gameDesignerTitle.SetText(languageText.GameDesignerTitleCreditsKey);
    }
}