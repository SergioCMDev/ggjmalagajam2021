﻿using App.PlayerModelInfo;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Utils.Input;

public class CanvasPuzzlePresenter : MonoBehaviour
{
    [SerializeField] private VictoryPanelView _victoryPanelView;
    [SerializeField] private PauseMenuView _pauseMenuView;
    [SerializeField] private TutorialView _tutorialView;
    [SerializeField] private Button _buttonPause;
    [SerializeField] private Image _fade;
    private IEventManager _eventManager;
    private ILanguageManager _languageManager;
    private SceneChanger _sceneChanger;
    private ReadInputPlayer _readInputPlayer;
    private IPlayerModel _playerModel;


    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
    }

    private void Start()
    {
        InitializeViewsAndScene();
        _readInputPlayer.ReleaseCursor();
        _readInputPlayer.ShowCursor();

        _eventManager.AddActionToSignal<PlayerHasCompletedPuzzleSignal>(HandleVictory);
        _victoryPanelView.OnPlayerPressContinueButton += ContinueToHouseScene;

        _buttonPause.onClick.AddListener(PauseGame);
        _victoryPanelView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _pauseMenuView.SetLanguageTranslations(_languageManager.GetActualLanguageText());

        _pauseMenuView.OnExitLevel += GoToInitialScene;
        _pauseMenuView.OnRestartGame += RestartLevel;
        _pauseMenuView.OnResumeGame += ResumeLevel;

        if (!_tutorialView) return;
        _tutorialView.OnTutorialStartButtonPressed += StartGame;
        StartTutorial();
    }

    private void StartGame()
    {
        ResumeTimeScale();
        _tutorialView.gameObject.SetActive(false);
    }

    private void StartTutorial()
    {
        PauseTimeScale();

        _tutorialView.gameObject.SetActive(true);
        _tutorialView.SetLanguageTranslations(_languageManager.GetActualLanguageText());
    }

    private void InitializeViewsAndScene()
    {
        ResumeTimeScale();
        _fade.gameObject.SetActive(false);
        _buttonPause.gameObject.SetActive(true);

        _pauseMenuView.gameObject.SetActive(false);
        _victoryPanelView.gameObject.SetActive(false);
        if (_tutorialView)
            _tutorialView.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayerHasCompletedPuzzleSignal>(HandleVictory);
        _victoryPanelView.OnPlayerPressContinueButton -= ContinueToHouseScene;

        _pauseMenuView.OnExitLevel -= GoToInitialScene;
        _pauseMenuView.OnRestartGame -= RestartLevel;
        _pauseMenuView.OnResumeGame -= ResumeLevel;

        _buttonPause.onClick.RemoveListener(PauseGame);
    }

    private void ResumeTimeScale()
    {
        Time.timeScale = 1;
    }

    private void ResumeLevel()
    {
        _buttonPause.gameObject.SetActive(true);
        _fade.gameObject.SetActive(false);

        _pauseMenuView.gameObject.SetActive(false);
        ResumeTimeScale();
    }

    private void RestartLevel()
    {
        _sceneChanger.RestartScene();
    }

    private void GoToInitialScene()
    {
        _sceneChanger.GoToInitScene();
    }

    private void PauseGame()
    {
        _fade.gameObject.SetActive(true);
        _buttonPause.gameObject.SetActive(false);
        _pauseMenuView.gameObject.SetActive(true);
        PauseTimeScale();
    }

    private void PauseTimeScale()
    {
        Time.timeScale = 0;
    }

    private void ContinueToHouseScene()
    {
        _sceneChanger.GoToHouseScene();
    }

    private void HandleVictory(PlayerHasCompletedPuzzleSignal obj)
    {
        _buttonPause.gameObject.SetActive(false);
        PauseTimeScale();
        _playerModel.AddPassedPuzzle(_sceneChanger.GetCurrentScene());
        _fade.gameObject.SetActive(true);
        _victoryPanelView.gameObject.SetActive(true);
    }
}