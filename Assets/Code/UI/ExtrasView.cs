﻿using App.PlayerModelInfo;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class ExtrasView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _descubre1Title, _descubre2Title;
    [SerializeField] private Image _puzzle2D1, _puzzle2D2;
    [SerializeField] private Button _backButton;

    private IPlayerModel _playerModel;

    public event Action OnBackButtonPressed;

    void Start()
    {
        _backButton.onClick.AddListener(() => OnBackButtonPressed.Invoke());

        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();

        //Si los puzzles están resueltos, mostrar imágenes y ocultar textos.
        if (_playerModel.ContainsPuzzleData("Puzzle2D1")) {
            _puzzle2D1.gameObject.SetActive(true);
            _descubre1Title.gameObject.SetActive(false);
        }
        if (_playerModel.ContainsPuzzleData("Puzzle2D2")) {
            _puzzle2D2.gameObject.SetActive(true);
            _descubre2Title.gameObject.SetActive(false);
        }

    }

    private void OnDestroy() {
        _backButton.onClick.RemoveListener(() => OnBackButtonPressed.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _descubre1Title.SetText(languageText.DiscoverTextExtrasKey);
        _descubre2Title.SetText(languageText.DiscoverTextExtrasKey);
    }
}