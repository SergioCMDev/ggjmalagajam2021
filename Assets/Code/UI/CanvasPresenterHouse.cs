﻿using System;
using App.PlayerModelInfo;
using Presentation.Items;
using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

public class CanvasPresenterHouse : MonoBehaviour
{
    [SerializeField] private CatChatBot _catChatBot;
    [SerializeField] private PauseMenuView _pauseMenuView;
    [SerializeField] private TutorialHouseView _tutorialHouseView;
    [SerializeField] private Fader _fader;
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private EndAnimation _endAnimation;
    [SerializeField] private Transform _endAnimationObj;
    [SerializeField] private Transform _cameraVirtual;
    [SerializeField] private bool _doEnd;


    private ILanguageManager _languageManager;
    private SceneChanger _sceneChanger;
    private IEventManager _eventManager;
    private IPlayerModel _playerModel;
    private ReadInputPlayer _readInputPlayer;

    private bool _initTutorialIsShowed;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
    }

    void Start()
    {
        _initTutorialIsShowed = false;
        _eventManager.AddActionToSignal<ShowCatChatBoxSignal>(ShowCatChatBox);
        _eventManager.AddActionToSignal<HideCatChatBoxSignal>(HideCatChatBox);
        _readInputPlayer.OnPlayerPressEscapeKey += PauseGame;
        _pauseMenuView.OnResumeGame += ResumeGame;
        _pauseMenuView.OnExitLevel += ExitLevel;
        _pauseMenuView.OnRestartGame += RestartLevel;
        _fader.OnChangeSceneToCredits += ChangeSceneToCredits;

        _readInputPlayer.BlockCursor();
        _readInputPlayer.HideCursor();
        ResumeTimeScale();

        _tutorialHouseView.gameObject.SetActive(false);

        if (!_playerModel.TutorialHouseShowed() && !_playerModel.PlayerComesFromPuzzle())
        {
            _initTutorialIsShowed = true;
            PauseTimeScale();
            _readInputPlayer.ShowCursor();
            _readInputPlayer. ReleaseCursor();
            ShowTutorial();
            _tutorialHouseView.OnContinueButtonPressed += StartGamePlayHouse;
        }

        //_playerModel.AddPassedPuzzle("uno");
        //_playerModel.AddPassedPuzzle("dos");
        //_playerModel.AddPassedPuzzle("tres");
        //_playerModel.AddPassedPuzzle("cuatro");


        _catChatBot.gameObject.SetActive(false);
    }

    private void ChangeSceneToCredits()
    {
        _sceneChanger.GoToCreditsScene();
    }

    private void Update()
    {
        if (_doEnd || _playerModel.NumOfPuzzlesCompleted() == 4)
        {
            _doEnd = false;
            _playerTransform.position = _endAnimationObj.transform.position;
            _readInputPlayer.DisableGameplayInput();
            _readInputPlayer.DisableMouseInput();
            new StopRotationCameraSignal().Execute();
            _cameraVirtual.rotation = _endAnimationObj.transform.localRotation;
            //Ocultar al jugador
            _playerTransform.GetComponent<MeshRenderer>().enabled = false;
            _fader.gameObject.SetActive(true);

            _endAnimation.StartEndAnimations();
        }
    }

    private void StartGamePlayHouse()
    {
        _tutorialHouseView.gameObject.SetActive(false);
        _readInputPlayer.BlockCursor();
        _readInputPlayer.HideCursor();
        ResumeTimeScale();
        _initTutorialIsShowed = false;
    }

    private void ShowTutorial()
    {
        _tutorialHouseView.gameObject.SetActive(true);
        _tutorialHouseView.SetLanguageTranslations(_languageManager.GetActualLanguageText());
        _playerModel.SetTutorialHouseStatus(true);
    }

    private void PauseGame()
    {
        if (_initTutorialIsShowed) return;

        _pauseMenuView.gameObject.SetActive(true);
        _pauseMenuView.SetLanguageTranslations(_languageManager.GetActualLanguageText());
        PauseTimeScale();
        _readInputPlayer.ReleaseCursor();
        _readInputPlayer.ShowCursor();
    }


    private void RestartLevel()
    {
        _sceneChanger.RestartScene();
    }

    private void ExitLevel()
    {
        _sceneChanger.GoToInitScene();
    }

    private void ResumeGame()
    {
        _pauseMenuView.gameObject.SetActive(false);
        ResumeTimeScale();
        _readInputPlayer.BlockCursor();
        _readInputPlayer.HideCursor();
    }

    private void PauseTimeScale()
    {
        Time.timeScale = 0;
    }

    private void ResumeTimeScale()
    {
        Time.timeScale = 1;
    }


    private void HideCatChatBox(HideCatChatBoxSignal obj)
    {
        _catChatBot.gameObject.SetActive(false);
    }

    private void ShowCatChatBox(ShowCatChatBoxSignal obj)
    {
        _catChatBot.gameObject.SetActive(true);

        _catChatBot.SetText(obj.Text);
        _catChatBot.SetImage(obj.Sprite);
        _catChatBot.SetCatName(obj.SpriteCatName);
    }
}