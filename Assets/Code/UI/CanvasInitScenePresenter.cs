﻿using App.PlayerModelInfo;
using TMPro;
using UnityEngine;
using Utils;

public class CanvasInitScenePresenter : MonoBehaviour
{
    [SerializeField] private InitSceneView _initSceneView;
    [SerializeField] private OptionsView _optionsView;
    [SerializeField] private CreditsView _creditsView;
    [SerializeField] private ExtrasView _extrasView;
    [SerializeField] private TextMeshProUGUI _versionText;
    private AudioManagerBase _audioManagerBase;

    private ILanguageManager _languageManager;
    private SceneChanger _sceneChanger;

    void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _audioManagerBase = ServiceLocator.Instance.GetService<AudioManagerBase>();
    }

    void Start()
    {
        SetVersionInfo();

        ShowInitView();
        _optionsView.gameObject.SetActive(false);
        _creditsView.gameObject.SetActive(false);
        _extrasView.gameObject.SetActive(false);


        _optionsView.OnBackButtonPressed += HandleOptionsCharactersBackButton;
        _optionsView.OnLanguageChanged += HandleLanguageChanged;
        _optionsView.OnBackgroundVolumeChanged += BackgroundVolumeChanged;
        _optionsView.OnMasterVolumeChanged += MasterVolumeChanged;
        _optionsView.OnVFXVolumeChanged += VFXVolumeChanged;

        _creditsView.OnBackButtonPressed += HandleCreditsBackButton;
        _extrasView.OnBackButtonPressed += HandleExtrasBackButton;


        _initSceneView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _initSceneView.OnOptionsButtonPressed += ShowOptions;
        _initSceneView.OnCreditsButtonPressed += ShowCredits;
        _initSceneView.OnExtrasButtonPressed += ShowExtras;
        _initSceneView.OnPlayButtonPressed += InitPlayMode;
        _initSceneView.OnExitButtonPressed += ExitGame;
    }

    private void MasterVolumeChanged(bool status)
    {
        _audioManagerBase.UpdateMasterVolume(status);
    }

    private void BackgroundVolumeChanged(bool status)
    {
        _audioManagerBase.UpdateBackgroundVolume(status);
    }

    private void HandleLanguageChanged(string obj)
    {
        LanguagesKeys languagesKeysToChange = _languageManager.GetLanguageKeyByName(obj);
        _languageManager.SetActualLanguageText(languagesKeysToChange);
        _optionsView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        SetVersionInfo();
    }

    private void VFXVolumeChanged(bool status)
    {
        _audioManagerBase.UpdateVFXVolume(status);
    }


    private void SetVersionInfo()
    {
        _versionText.SetText($"v{Application.version}");
    }


    private void HandleOptionsCharactersBackButton()
    {
        _optionsView.gameObject.SetActive(false);
        ShowInitView();
    }

    private void HandleCreditsBackButton()
    {
        _creditsView.gameObject.SetActive(false);
        ShowInitView();
    }

    private void HandleExtrasBackButton()
    {
        _extrasView.gameObject.SetActive(false);
        ShowInitView();
    }


    private void ShowInitView()
    {
        // new PlayClickButtonSignal().Execute();

        _initSceneView.SetLanguageStrings(_languageManager.GetActualLanguageText());

        _initSceneView.gameObject.SetActive(true);
    }

    private void HideInitView()
    {
        _initSceneView.gameObject.SetActive(false);
    }


    private void ExitGame()
    {
        // new PlayClickButtonSignal().Execute();

        Application.Quit();
    }

    private void InitPlayMode()
    {
        // new PlayClickButtonSignal().Execute();
        // HideInitView();
        _sceneChanger.GoToHouseScene();
    }

    private void ShowCredits()
    {
        // new PlayClickButtonSignal().Execute();
        Debug.Log("Fufa ShowCredits");
        HideInitView();
        _creditsView.gameObject.SetActive(true);
        _creditsView.SetLanguageStrings(_languageManager.GetActualLanguageText());
    }

    private void ShowOptions()
    {
        // new PlayClickButtonSignal().Execute();

        HideInitView();
        _optionsView.gameObject.SetActive(true);
        _optionsView.SetLanguageStrings(_languageManager.GetActualLanguageText());
        _optionsView.SetDropdownValues(_languageManager.GetLanguageNames(), _languageManager.GetActualLanguageKey());
    }

    private void ShowExtras()
    {
        // new PlayClickButtonSignal().Execute();

        HideInitView();
        _extrasView.gameObject.SetActive(true);
        _extrasView.SetLanguageStrings(_languageManager.GetActualLanguageText());
    }
}