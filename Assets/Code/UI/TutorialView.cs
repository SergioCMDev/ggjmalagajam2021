﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _startButtonText,
        _titleTutorialText,
        _pieceText,
        _rotateTitleText,
        _titleMenuItemsText,
        _moveTitleMenuText,
        _changeOrientationMenuText,
        _clickSlideMenuText,
        _rightClickMenuText;

    [SerializeField] private Button _button;

    public event Action OnTutorialStartButtonPressed = delegate { };

    void Start()
    {
        _button.onClick.AddListener(() => OnTutorialStartButtonPressed.Invoke());
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(() => OnTutorialStartButtonPressed.Invoke());
    }


    public void SetLanguageTranslations(LanguageText languageText)
    {
        _startButtonText.SetText(languageText.StartGameMenuTutorial);
        _titleTutorialText.SetText(languageText.TitleTutorialMenuInGameKey);
        _pieceText.SetText(languageText.ItemsTitleTutorialKey);
        _rotateTitleText.SetText(languageText.RotateTitleTutorialKey);
        _titleMenuItemsText.SetText(languageText.ItemsTitleTutorialKey);
        _moveTitleMenuText.SetText(languageText.MoveTitleTutorialKey);
        _changeOrientationMenuText.SetText(languageText.ChangeOrientationTitleTutorialKey);
        _clickSlideMenuText.SetText(languageText.ClickSlideTutorialKey);
        _rightClickMenuText.SetText(languageText.RightClickMenuTutorialKey);
    }
}