﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.HID;
using UnityEngine.UI;

public class VictoryPanelView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _titlePanel, _buttonContinueText;
    [SerializeField] private Button _button;

    public event Action OnPlayerPressContinueButton = delegate { };

    // Start is called before the first frame update
    void Start()
    {
        _button.onClick.AddListener(()=>OnPlayerPressContinueButton.Invoke());
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(()=>OnPlayerPressContinueButton.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _titlePanel.SetText(languageText.TitlePanelVictoryViewPuzzleScene);
        _buttonContinueText.SetText(languageText.ButtonContinuePanelVictoryViewPuzzleScene);

    }
}
