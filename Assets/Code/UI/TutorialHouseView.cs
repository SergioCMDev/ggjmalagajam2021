﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialHouseView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _titleTutorial, _infoTutorial, _buttonContinueText;
    [SerializeField] private Button _buttonContinue;

    public event Action OnContinueButtonPressed = delegate { };

    private void Start()
    {
        _buttonContinue.onClick.AddListener(() => OnContinueButtonPressed.Invoke());
    }

    private void OnDestroy()
    {
        _buttonContinue.onClick.RemoveListener(() => OnContinueButtonPressed.Invoke());
    }

    public void SetLanguageTranslations(LanguageText languageText)
    {
        _titleTutorial.SetText(languageText.TutorialTitleHouseKey);
        _infoTutorial.SetText(languageText.TutorialInfoHouseKey);
        _buttonContinueText.SetText(languageText.ButtonContinueTutorialHouseTextKey);
    }
}