﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InitSceneView : MonoBehaviour
{
    [SerializeField] private Button _playModeButton,
        _optionsButton,
        _creditsButton,
        _extrasButton,
        _exitButton;

    [SerializeField] private TextMeshProUGUI _playModeButtonText,
        _optionsButtonText,
        _creditsButtonText,
        _extrasButtonText,
        _exitButtonText;

    public event Action OnPlayButtonPressed,
        OnCreditsButtonPressed,
        OnOptionsButtonPressed,
        OnExtrasButtonPressed,
        OnExitButtonPressed;

    void Start()
    {
        _playModeButton.onClick.AddListener(() => OnPlayButtonPressed.Invoke());
        _creditsButton.onClick.AddListener(() => OnCreditsButtonPressed.Invoke());
        _optionsButton.onClick.AddListener(() => OnOptionsButtonPressed.Invoke());
        _extrasButton.onClick.AddListener(() => OnExtrasButtonPressed.Invoke());
        _exitButton.onClick.AddListener(() => OnExitButtonPressed.Invoke());
    }

    private void OnDestroy()
    {
        _playModeButton.onClick.RemoveListener(() => OnPlayButtonPressed.Invoke());
        _creditsButton.onClick.RemoveListener(() => OnCreditsButtonPressed.Invoke());
        _optionsButton.onClick.RemoveListener(() => OnOptionsButtonPressed.Invoke());
        _extrasButton.onClick.RemoveListener(() => OnExtrasButtonPressed.Invoke());
        _exitButton.onClick.RemoveListener(() => OnExitButtonPressed.Invoke());
    }

    public void SetLanguageStrings(LanguageText languageText)
    {
        _playModeButtonText.SetText(languageText.PlayButtonInitScene);
        _creditsButtonText.SetText(languageText.CreditsButtonInitScene);
        _optionsButtonText.SetText(languageText.OptionsButtonInitScene);
        _extrasButtonText.SetText(languageText.ExtrasButtonInitScene);
        _exitButtonText.SetText(languageText.ExitButtonInitScene);
    }
}