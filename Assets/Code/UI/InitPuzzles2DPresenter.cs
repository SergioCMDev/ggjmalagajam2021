﻿using App.PlayerModelInfo;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Utils;

public class InitPuzzles2DPresenter : MonoBehaviour
{

    [SerializeField] private InitSceneView _initSceneView;
    [SerializeField] private OptionsView _optionsView;
    [SerializeField] private CreditsView _creditsView;
    [SerializeField] private ExtrasView _extrasView;
    [SerializeField] private TextMeshProUGUI _versionText;
    [SerializeField] private AudioManagerBase _audioManagerBase;

    private IPlayerModel _playerModel;
    private IAudioModel _audioModel;
    private UpdatePlayerProgression _updatePlayerProgression;
    private ILanguageManager _languageManager;
    private SceneChanger _sceneChanger;

    void Awake() {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }




}
