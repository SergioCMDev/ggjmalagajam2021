﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CatChatBot : MonoBehaviour
{
    [SerializeField] private Image _image, _imageCatName;

    [SerializeField] private TextMeshProUGUI _text;

    // Start is called before the first frame update
    public void SetImage(Sprite sprite)
    {
        _image.sprite = sprite;
    }

    public void SetText(string text)
    {
        _text.SetText(text);
    }

    public void SetCatName(Sprite spriteCatName)
    {
        _imageCatName.sprite = spriteCatName;
    }
}