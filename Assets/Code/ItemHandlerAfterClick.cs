﻿using UnityEngine;

public class ItemHandlerAfterClick : MonoBehaviour
{
    [SerializeField] private CameraClickerThirdPerson cameraClickerThirdPerson;
    private Transform _itemInteaction;

    private void Start()
    {
        cameraClickerThirdPerson.OnPlayerInteractWithItem += HandleItem;
        cameraClickerThirdPerson.OnPlayerStopInteractionWithItem += StopHandleItem;
    }

    private void StopHandleItem()
    {
        if (!_itemInteaction) return;
        _itemInteaction.GetComponent<ISelectable>().Deselect();
        _itemInteaction = null;
    }

    private void HandleItem(ObjectInfo obj)
    {
        var selectable = obj.gameObject.GetComponent<ISelectable>();
        if (selectable == null) return;
        selectable.SetData(obj.offset);
        selectable.Select();

        _itemInteaction = obj.gameObject.transform;
    }
}