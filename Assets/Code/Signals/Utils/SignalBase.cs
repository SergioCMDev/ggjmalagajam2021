﻿using Signals.EventManager;
using Utils;

namespace Signals
{
    public abstract class SignalBase
    {
        public void Execute()
        {
            ServiceLocator.Instance.GetService<IEventManager>().NotifySubscribers(this);
        }
    }
}