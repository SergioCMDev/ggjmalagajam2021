﻿using Domain;
using Domain.LoaderSaveGame;
using Domain.SaverGame;
using Signals.EventManager;
using Utils;

public class UpdatePlayerProgression
{
    private IEventManager _eventManager;
    private ISaver _saver;
    private ILoader _loader;

    public UpdatePlayerProgression()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _saver = ServiceLocator.Instance.GetService<ISaver>();
        _loader = ServiceLocator.Instance.GetService<ILoader>();
    }

    public void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<SavePlayerProgressSignal>(SavePlayerProgress);
    }

    private void SavePlayerProgress(SavePlayerProgressSignal signal)
    {
        SaveGameData infoToSave = _loader.LoadGame();
        infoToSave.AddInfo(signal.IdSceneToSave);
        _saver.SaveGame(infoToSave);
    }

    public void Init()
    {
        _eventManager.AddActionToSignal<SavePlayerProgressSignal>(SavePlayerProgress);
    }
}