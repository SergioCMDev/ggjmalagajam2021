﻿using System;

namespace Domain
{
    [Serializable]
    public class SaveGameData
    {
        public int LastLevelCompletedSaved;

        public SaveGameData()
        {
            LastLevelCompletedSaved = -1;
        }

        public void AddInfo(int lastLevelCompleted)
        {
            bool HasToUpgradeLastCompletedLevel()
            {
                return LastLevelCompletedSaved < lastLevelCompleted;
            }

            if (lastLevelCompleted < 0)
            {
                throw new Exception("Last level completed to save is < 0");
            }


            if (ExistsSaveGame())
            {
                if (!HasToUpgradeLastCompletedLevel())
                {
                    return;
                }
            }

            LastLevelCompletedSaved = lastLevelCompleted;
        }

        private bool ExistsSaveGame()
        {
            return LastLevelCompletedSaved >= 0;
        }
    }
}