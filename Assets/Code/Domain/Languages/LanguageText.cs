﻿using System.Collections.Generic;
using UnityEngine;

public abstract class LanguageText : ScriptableObject
{
    public string ExitButtonInitScene;
    public string PlayButtonInitScene;
    public string OptionsButtonInitScene;
    public string ExtrasButtonInitScene;
    public string CreditsButtonInitScene;
    public string ExitButtonMenuInGameKey;
    public string RestartButtonMenuInGameKey;
    public string ResumeButtonPauseMenuInGameKey;
    public string TitleGameOverMenuInGameKey;
    public string TitlePauseMenuInGameKey;
    public string Artist2DTitleCreditsKey;
    public string Artist3DTitleCreditsKey;
    public string CompositorsVFXTitleCreditsKey;
    public string ArtistVFXTitleCreditsKey;
    public string ProgrammersTitleCreditsKey;
    public string TitleCreditsSceneCreditsKey;
    public string GameDesignerTitleCreditsKey;
    public string ExitButtonSceneCreditsKey;
    public string DiscoverTextExtrasKey;
    public string StoreButtonInitScene;
    public string AudioTextInitScene;
    public string LanguageTextInitScene;
    public string OptionsTitleInitScene;
    public string AudioBackgroundTextInitScene;
    public string AudioTitleInitScene;
    public string AudioMasterTextInitScene;
    public string AudioVFXTextInitScene;
    public string TitlePanelVictoryViewPuzzleScene;
    public string ButtonContinuePanelVictoryViewPuzzleScene;
    public string ItemsTitleTutorialKey;

    public string MoveTitleTutorialKey;
    public string StartGameMenuTutorial;
    public string TitleTutorialMenuInGameKey;
    public string ChangeOrientationTitleTutorialKey;
    public string ClickSlideTutorialKey;
    public string RightClickMenuTutorialKey;
    public string RotateTitleTutorialKey;

    public List<CatPhraseKey> _catPhraseses;
    public string TutorialTitleHouseKey;
    public string TutorialInfoHouseKey;
    public string ButtonContinueTutorialHouseTextKey;
}