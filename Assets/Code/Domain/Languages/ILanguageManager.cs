﻿using System.Collections.Generic;

public interface ILanguageManager
{
    LanguageText GetActualLanguageText();
    LanguagesKeys GetActualLanguageKey();
    LanguagesKeys GetLanguageKeyByName(string languagesNameToChange);
    List<LanguageName> GetLanguageNames();
    void SetActualLanguageText(LanguagesKeys languagesKeyToChange);
    string GetCatTextByKey(string keyTextToSay);
}