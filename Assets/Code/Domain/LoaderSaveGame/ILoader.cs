﻿namespace Domain.LoaderSaveGame
{
    public interface ILoader
    {
        SaveGameData LoadGame();
        bool HasSavedGame();
    }
}