﻿using System;
using InputPlayerSystem;
using UnityEngine;
using UnityEngine.InputSystem;


namespace Utils.Input
{
    public class ReadInputPlayer : MonoBehaviour
    {
        private GameplayInput _playerInputActions;
        private Vector2 _movementInMenuInput;
        private Vector2 _mouseMovementInput;

        public event Action OnPlayerPressEscapeKey = delegate { };

        public event Action OnPlayerPressLeftButtonMouse = delegate { };
        public event Action OnPlayerReleaseLeftButtonMouse = delegate { };
        public event Action OnPlayerPressControlButton = delegate { };
        public event Action OnPlayerPressRightButtonMouse = delegate { };
        public event Action OnPlayerReleaseRightButtonMouse = delegate { };

        // public event Action OnPlayerPressEnterButton = delegate { };
        // public event Action OnDebugEscPressed = delegate { };

        public Vector3 MovementInGameAxis { get; private set; }
        public Vector3 MouseMovement { get; private set; }

        private void OnEnable()
        {
            _playerInputActions?.Enable();
        }

        private void OnDisable()
        {
            _playerInputActions?.Disable();
        }

        private void Awake()
        {
            _playerInputActions = new GameplayInput();


            _playerInputActions.Gameplay.Movement.performed += PlayerMove;
            _playerInputActions.Gameplay.Movement.canceled += PlayerStopMoving;

            _playerInputActions.Gameplay.Interact.performed += ctx => ControlPushed();
            _playerInputActions.Gameplay.Escape.performed += ctx => OnPlayerPressEscapeKey();
            
            _playerInputActions.Mouse.RightClickItem.performed += ctx => ClickRightMouse();
            _playerInputActions.Mouse.RightClickItem.canceled += ctx => ReleaseRightClickMouse();


            _playerInputActions.Mouse.LeftClickItem.performed += ctx => ClickLeftMouse();
            _playerInputActions.Mouse.LeftClickItem.canceled += ctx => ReleaseLeftClickMouse();


            _playerInputActions.Mouse.Rotation.performed += RotateMouse;
            _playerInputActions.Mouse.Rotation.canceled += StopRotateMouse;
        }


        private void OnDestroy()
        {
            if (_playerInputActions != null)
            {
                // _playerInputActions.Gameplay.Interact.performed -= ctx => InteractOneTime(); //PRESS ONE TIME
                _playerInputActions.Gameplay.Interact.canceled -= ctx => InteractReleased(); //PRESS AND RELEASE

                _playerInputActions.Gameplay.Movement.performed -= PlayerMove;
                _playerInputActions.Gameplay.Movement.canceled -= PlayerStopMoving;

                // _playerInputActions.Gameplay.AttackRight.performed += ctx => AttackRight();
                _playerInputActions.Mouse.RightClickItem.performed -= ctx => ClickRightMouse();
                _playerInputActions.Mouse.RightClickItem.canceled -= ctx => ReleaseRightClickMouse();
            }
        }
        
        public void HideCursor()
        {
            Cursor.visible = false;
        }

        public void ShowCursor()
        {
            Cursor.visible = true;
        }

        public void ReleaseCursor()
        {
            Cursor.lockState = CursorLockMode.None;
        }

        public void BlockCursor()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void ReleaseLeftClickMouse()
        {
            OnPlayerReleaseLeftButtonMouse.Invoke();
        }

        private void ClickLeftMouse()
        {
            OnPlayerPressLeftButtonMouse.Invoke();
        }

        private void ClickRightMouse()
        {
            OnPlayerPressRightButtonMouse.Invoke();
        }

        private void ReleaseRightClickMouse()
        {
            OnPlayerReleaseRightButtonMouse.Invoke();
        }


        private void PlayerStopMoving(InputAction.CallbackContext obj)
        {
            this.MovementInGameAxis = Vector3.zero;
        }

        private void PlayerMove(InputAction.CallbackContext obj)
        {
            if (!(obj.ReadValue<Vector2>().magnitude > 0.01f)) return;
            Vector2 vectorReceived = obj.ReadValue<Vector2>();
            MovementInGameAxis = new Vector3(vectorReceived.x, 0, vectorReceived.y);
        }

        private void RotateMouse(InputAction.CallbackContext obj)
        {
            if (obj.ReadValue<Vector2>().magnitude < 0.01f) return;
            MouseMovement = obj.ReadValue<Vector2>();
            // OnPlayerMovesMouse.Invoke(MouseMovement);
        }


        private void StopRotateMouse(InputAction.CallbackContext obj)
        {
            MouseMovement = Vector2.zero;

            // OnPlayerStopMovesMouse.Invoke(MouseMovement);
        }


        private void ControlPushed()
        {
            OnPlayerPressControlButton.Invoke();
        }


        private void InteractReleased()
        {
            Debug.Log("RELEASE");
        }

        public void DisableGameplayInput()
        {
            _playerInputActions.Gameplay.Disable();
        }

        public void EnableGameplayInput()
        {
            _playerInputActions.Gameplay.Enable();
        }

        public void DisableMouseInput()
        {
            _playerInputActions.Mouse.Disable();
        }

        public void EnableMouseInput()
        {
            _playerInputActions.Mouse.Enable();
        }
    }
}