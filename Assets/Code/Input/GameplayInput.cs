// GENERATED AUTOMATICALLY FROM 'Assets/Code/Input/GameplayInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace InputPlayerSystem
{
    public class @GameplayInput : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @GameplayInput()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameplayInput"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""58947089-b719-4b23-987e-fea0f6006aa4"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""8c0db3f0-f5e7-442b-ad31-5fd7773c7ccf"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""319b9613-8c83-4704-9723-37ebb0dab2b0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""f11dc4a6-37cd-47e9-8157-ec40fa48aab1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PauseGame"",
                    ""type"": ""Button"",
                    ""id"": ""a783d080-fddd-47f0-a051-f36f0e4b3e7e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Escape"",
                    ""type"": ""Button"",
                    ""id"": ""6f8c1ed2-dddb-480a-8e2f-26f27878a09f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""543f9201-bdd2-45c9-a4fa-78c3983d5eac"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""f491ae78-3fb7-4d16-b7e3-d16d0c4cd9f5"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""e472434e-4cb0-4832-9806-4c1b8f5096e1"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""4e4577c7-a030-49a6-a254-eb600694a1ec"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""c30dfeb4-21b0-40bd-97ab-63161ebb5946"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""WASDGamepad"",
                    ""id"": ""064160fe-f260-4b9a-a314-508188d087f9"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""5950039d-d70c-44de-8e2a-45ec6fdcfd26"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""4cb3f41d-6638-4ac3-840b-a528849a7235"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""8569778f-2e31-4f09-825b-31a450c3b5ad"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""869c52a2-8644-4611-a659-6e6af65f2ad2"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""3a2dfa23-f1cb-48e3-8e06-32e17f0cc5a2"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9dd55e95-0136-473b-8c7e-8c2c7f27aeab"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3b7f6963-95d0-48e4-a109-1f903257e788"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PauseGame"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5a1ebd6a-6a14-410e-a6a8-f53d7acebb03"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Escape"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Mouse"",
            ""id"": ""5a2a4bf2-c7ae-4f39-bff8-ddd6834209f9"",
            ""actions"": [
                {
                    ""name"": ""Rotation"",
                    ""type"": ""Value"",
                    ""id"": ""59e8bbe7-846f-40c1-897c-dfbb71bc50ec"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightClickItem"",
                    ""type"": ""Button"",
                    ""id"": ""692bc074-a225-4973-9d3c-b5633c2ee39d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LeftClickItem"",
                    ""type"": ""Button"",
                    ""id"": ""76495b27-3df8-4e53-b280-a224b9650324"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b5b6ea3e-e4e1-4405-96ce-9692d4a82d71"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e956e1a4-8fd9-4e6a-b3ba-4495be358bd3"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightClickItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d89f1ca-853e-4c7e-bcb6-f3f9dd4c1596"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftClickItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Debug"",
            ""id"": ""0cc3789d-57e4-42c0-9ce5-2d756a6237eb"",
            ""actions"": [
                {
                    ""name"": ""EscapeKey"",
                    ""type"": ""Button"",
                    ""id"": ""03b27efe-f56a-4ae4-bb4e-8b288e819052"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""1c36a283-35fc-4cb4-be60-6eeffb349283"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""EscapeKey"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Gameplay
            m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
            m_Gameplay_Movement = m_Gameplay.FindAction("Movement", throwIfNotFound: true);
            m_Gameplay_Jump = m_Gameplay.FindAction("Jump", throwIfNotFound: true);
            m_Gameplay_Interact = m_Gameplay.FindAction("Interact", throwIfNotFound: true);
            m_Gameplay_PauseGame = m_Gameplay.FindAction("PauseGame", throwIfNotFound: true);
            m_Gameplay_Escape = m_Gameplay.FindAction("Escape", throwIfNotFound: true);
            // Mouse
            m_Mouse = asset.FindActionMap("Mouse", throwIfNotFound: true);
            m_Mouse_Rotation = m_Mouse.FindAction("Rotation", throwIfNotFound: true);
            m_Mouse_RightClickItem = m_Mouse.FindAction("RightClickItem", throwIfNotFound: true);
            m_Mouse_LeftClickItem = m_Mouse.FindAction("LeftClickItem", throwIfNotFound: true);
            // Debug
            m_Debug = asset.FindActionMap("Debug", throwIfNotFound: true);
            m_Debug_EscapeKey = m_Debug.FindAction("EscapeKey", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Gameplay
        private readonly InputActionMap m_Gameplay;
        private IGameplayActions m_GameplayActionsCallbackInterface;
        private readonly InputAction m_Gameplay_Movement;
        private readonly InputAction m_Gameplay_Jump;
        private readonly InputAction m_Gameplay_Interact;
        private readonly InputAction m_Gameplay_PauseGame;
        private readonly InputAction m_Gameplay_Escape;
        public struct GameplayActions
        {
            private @GameplayInput m_Wrapper;
            public GameplayActions(@GameplayInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Movement => m_Wrapper.m_Gameplay_Movement;
            public InputAction @Jump => m_Wrapper.m_Gameplay_Jump;
            public InputAction @Interact => m_Wrapper.m_Gameplay_Interact;
            public InputAction @PauseGame => m_Wrapper.m_Gameplay_PauseGame;
            public InputAction @Escape => m_Wrapper.m_Gameplay_Escape;
            public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
            public void SetCallbacks(IGameplayActions instance)
            {
                if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
                {
                    @Movement.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMovement;
                    @Movement.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMovement;
                    @Movement.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMovement;
                    @Jump.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                    @Jump.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                    @Jump.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                    @Interact.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                    @Interact.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                    @Interact.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                    @PauseGame.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPauseGame;
                    @PauseGame.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPauseGame;
                    @PauseGame.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPauseGame;
                    @Escape.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnEscape;
                    @Escape.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnEscape;
                    @Escape.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnEscape;
                }
                m_Wrapper.m_GameplayActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Movement.started += instance.OnMovement;
                    @Movement.performed += instance.OnMovement;
                    @Movement.canceled += instance.OnMovement;
                    @Jump.started += instance.OnJump;
                    @Jump.performed += instance.OnJump;
                    @Jump.canceled += instance.OnJump;
                    @Interact.started += instance.OnInteract;
                    @Interact.performed += instance.OnInteract;
                    @Interact.canceled += instance.OnInteract;
                    @PauseGame.started += instance.OnPauseGame;
                    @PauseGame.performed += instance.OnPauseGame;
                    @PauseGame.canceled += instance.OnPauseGame;
                    @Escape.started += instance.OnEscape;
                    @Escape.performed += instance.OnEscape;
                    @Escape.canceled += instance.OnEscape;
                }
            }
        }
        public GameplayActions @Gameplay => new GameplayActions(this);

        // Mouse
        private readonly InputActionMap m_Mouse;
        private IMouseActions m_MouseActionsCallbackInterface;
        private readonly InputAction m_Mouse_Rotation;
        private readonly InputAction m_Mouse_RightClickItem;
        private readonly InputAction m_Mouse_LeftClickItem;
        public struct MouseActions
        {
            private @GameplayInput m_Wrapper;
            public MouseActions(@GameplayInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @Rotation => m_Wrapper.m_Mouse_Rotation;
            public InputAction @RightClickItem => m_Wrapper.m_Mouse_RightClickItem;
            public InputAction @LeftClickItem => m_Wrapper.m_Mouse_LeftClickItem;
            public InputActionMap Get() { return m_Wrapper.m_Mouse; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(MouseActions set) { return set.Get(); }
            public void SetCallbacks(IMouseActions instance)
            {
                if (m_Wrapper.m_MouseActionsCallbackInterface != null)
                {
                    @Rotation.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnRotation;
                    @Rotation.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnRotation;
                    @Rotation.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnRotation;
                    @RightClickItem.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnRightClickItem;
                    @RightClickItem.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnRightClickItem;
                    @RightClickItem.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnRightClickItem;
                    @LeftClickItem.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnLeftClickItem;
                    @LeftClickItem.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnLeftClickItem;
                    @LeftClickItem.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnLeftClickItem;
                }
                m_Wrapper.m_MouseActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Rotation.started += instance.OnRotation;
                    @Rotation.performed += instance.OnRotation;
                    @Rotation.canceled += instance.OnRotation;
                    @RightClickItem.started += instance.OnRightClickItem;
                    @RightClickItem.performed += instance.OnRightClickItem;
                    @RightClickItem.canceled += instance.OnRightClickItem;
                    @LeftClickItem.started += instance.OnLeftClickItem;
                    @LeftClickItem.performed += instance.OnLeftClickItem;
                    @LeftClickItem.canceled += instance.OnLeftClickItem;
                }
            }
        }
        public MouseActions @Mouse => new MouseActions(this);

        // Debug
        private readonly InputActionMap m_Debug;
        private IDebugActions m_DebugActionsCallbackInterface;
        private readonly InputAction m_Debug_EscapeKey;
        public struct DebugActions
        {
            private @GameplayInput m_Wrapper;
            public DebugActions(@GameplayInput wrapper) { m_Wrapper = wrapper; }
            public InputAction @EscapeKey => m_Wrapper.m_Debug_EscapeKey;
            public InputActionMap Get() { return m_Wrapper.m_Debug; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(DebugActions set) { return set.Get(); }
            public void SetCallbacks(IDebugActions instance)
            {
                if (m_Wrapper.m_DebugActionsCallbackInterface != null)
                {
                    @EscapeKey.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnEscapeKey;
                    @EscapeKey.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnEscapeKey;
                    @EscapeKey.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnEscapeKey;
                }
                m_Wrapper.m_DebugActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @EscapeKey.started += instance.OnEscapeKey;
                    @EscapeKey.performed += instance.OnEscapeKey;
                    @EscapeKey.canceled += instance.OnEscapeKey;
                }
            }
        }
        public DebugActions @Debug => new DebugActions(this);
        public interface IGameplayActions
        {
            void OnMovement(InputAction.CallbackContext context);
            void OnJump(InputAction.CallbackContext context);
            void OnInteract(InputAction.CallbackContext context);
            void OnPauseGame(InputAction.CallbackContext context);
            void OnEscape(InputAction.CallbackContext context);
        }
        public interface IMouseActions
        {
            void OnRotation(InputAction.CallbackContext context);
            void OnRightClickItem(InputAction.CallbackContext context);
            void OnLeftClickItem(InputAction.CallbackContext context);
        }
        public interface IDebugActions
        {
            void OnEscapeKey(InputAction.CallbackContext context);
        }
    }
}
