﻿using Presentation.Items;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;
using Utils.Input;

public class CameraClickerFirstPerson : CameraClicker
{
    private void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
    }

    void Start()
    {
        _readInputPlayer.OnPlayerPressLeftButtonMouse += InteractWithObject;
        _readInputPlayer.OnPlayerReleaseLeftButtonMouse += StopInteractWithObject;
        _camera = Camera.main;
    }

    private void OnDestroy()
    {
        _readInputPlayer.OnPlayerPressLeftButtonMouse -= InteractWithObject;
        _readInputPlayer.OnPlayerReleaseLeftButtonMouse -= StopInteractWithObject;
    }

    private InteractableItem _interactableItem;

    private void Update()
    {
        if (!CheckRaycasts(out var hit))
        {
            return;
        }

        if (_interactableItem)
        {
            return;
        }

        _interactableItem = hit.collider.GetComponent<InteractableItem>();
        if (!_interactableItem) return;
        _interactableItem.OverObject();
    }

    private bool CheckRaycasts(out RaycastHit hit)
    {
        Ray ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
        hit = new RaycastHit();

        Physics.Raycast(ray, out hit, _maxDistanceToCheckItems, _layersToAllowInteractions);
        if (!ThereAreRaycastToElements(ray))
        {
            StopOverlayOverObject();
            StopInteractWithObject();
            return false;
        }

        return true;
    }

    private void InteractWithObject()
    {
        Ray ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
        RaycastHit hit = new RaycastHit();

        Physics.Raycast(ray, out hit, _maxDistanceToCheckItems, _layersToPreventInteractions);
        if (!ThereAreRaycastToElements(ray)) return;

       if(!_interactableItem) return;
       _interactableItem.InteractWithObject();
    }

    protected virtual void StopInteractWithObject()
    {
        if (!_interactableItem) return;
        _interactableItem = null;
    }

    public  void StopOverlayOverObject()
    {
        if (!_interactableItem) return;
        _interactableItem.StopOverObject();
    }
}