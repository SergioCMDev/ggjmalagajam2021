﻿using UnityEngine;
using UnityEngine.InputSystem;
using Utils.Input;

public abstract class CameraClicker : MonoBehaviour
{
    [SerializeField] protected LayerMask _layersToAllowInteractions;
    [SerializeField] protected LayerMask _layersToPreventInteractions;
    [SerializeField] protected float _maxDistanceToCheckItems;
    protected ReadInputPlayer _readInputPlayer;
    protected Transform _gameObjectInFrontOfPlayer;


    [SerializeField] protected Camera _camera;
    protected bool ThereAreRaycastToElements(Ray ray)
    {
        return Physics.Raycast(ray, out var hit, _maxDistanceToCheckItems, _layersToAllowInteractions);
    }
}