﻿using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

public class CameraRotator : MonoBehaviour
{
    private ReadInputPlayer _readInputPlayer;

    [SerializeField] private PlayerMovement _playerMovement;
    [SerializeField] private float _mouseSensivity, _minRotationY, _maxRotationY;
    [SerializeField] private bool _canRotate;

    private IEventManager _eventManager;

    private float _xMouseValue, _yMouseValue;
    private float _xRotation, _yRotation;
    private Vector2 _mouseInput;

    void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();

        _eventManager.AddActionToSignal<StopRotationCameraSignal>(DisableCamRotate);
    }

    private void Start()
    {
        // Cursor.lockState = CursorLockMode.Locked;
        // Cursor.visible = false;

    }


    private void StatusCameraRotation()
    {
        if (_canRotate)
        {
            Cursor.lockState = CursorLockMode.None;
            _canRotate = false;
            Cursor.visible = true;
            return;
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        _canRotate = true;
    }


    void Update()
    {
        if (!_canRotate)
        {
            return;
        }

        GetRotationValuesCamera();
        RotateCamera();
    }

    private void GetRotationValuesCamera()
    {
        float xMouseValue = _readInputPlayer.MouseMovement.y * Time.deltaTime * _mouseSensivity;
        float yMouseValue = _readInputPlayer.MouseMovement.x * Time.deltaTime * _mouseSensivity;

        _xRotation -= xMouseValue;
        _yRotation -= yMouseValue;

        _xRotation = Mathf.Clamp(_xRotation, _minRotationY, _maxRotationY);
    }

    private void RotateCamera()
    {
        transform.localRotation = Quaternion.Euler(_xRotation, -_yRotation, 0.0f);
        _playerMovement.transform.rotation = Quaternion.Euler(_xRotation, -_yRotation, 0.0f);
    }

    private void DisableCamRotate(StopRotationCameraSignal obj) {
        _canRotate = false;
    }

}