﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;
using Utils.Input;


public class CameraClickerThirdPerson : CameraClicker
{
    private Vector3 _screenPoint;
    private Vector3 _offset;
    public event Action<ObjectInfo> OnPlayerInteractWithItem = delegate { };
    public event Action OnPlayerStopInteractionWithItem = delegate { };

    private void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
    }

    void Start()
    {
        _readInputPlayer.OnPlayerPressLeftButtonMouse += InteractWithObject;
        _readInputPlayer.OnPlayerReleaseLeftButtonMouse += StopInteractWithObject;
    }

    private void OnDestroy()
    {
        _readInputPlayer.OnPlayerPressLeftButtonMouse -= InteractWithObject;
        _readInputPlayer.OnPlayerReleaseLeftButtonMouse -= StopInteractWithObject;
    }
    

    private void InteractWithObject()
    {
        Ray ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
        RaycastHit hit = new RaycastHit();

        Physics.Raycast(ray, out hit, _maxDistanceToCheckItems, _layersToPreventInteractions);
        if (!ThereAreRaycastToElements(ray)) return;


        _gameObjectInFrontOfPlayer = hit.transform;
        _offset = hit.point - _gameObjectInFrontOfPlayer.transform.position;


        OnPlayerInteractWithItem.Invoke(new ObjectInfo()
        {
            gameObject = _gameObjectInFrontOfPlayer.transform,
            offset = _offset
        });
    }

    protected virtual void StopInteractWithObject()
    {
        if (!_gameObjectInFrontOfPlayer) return;

        _gameObjectInFrontOfPlayer = null;
        OnPlayerStopInteractionWithItem.Invoke();
    }
    
}