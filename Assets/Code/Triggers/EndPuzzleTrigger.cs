﻿using System;
using UnityEngine;

public class EndPuzzleTrigger : MonoBehaviour
{
    private bool _executed;

    private void Start()
    {
        _executed = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !_executed)
        {
            _executed = true;
            new PlayerHasCompletedPuzzleSignal().Execute();
            Debug.Log("Player");
        }
    }
}