﻿using App.PlayerModelInfo;
using UnityEngine;
using Utils;

public class TriggerToPuzzleMoveBox : MonoBehaviour
{
    [SerializeField] private string _puzzleSceneToLoad;
    [SerializeField] private Transform _positionToLoadAfterComplete;
    private SceneChanger _sceneChanger;
    private bool _executed;
    private IPlayerModel _playerModel;

    void Awake()
    {
        _executed = false;

        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        if (PassedPuzzle())
        {
            Destroy(gameObject);
        }
    }

    private bool PassedPuzzle()
    {
        return _playerModel.ContainsPuzzleData(_puzzleSceneToLoad);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !_executed)
        {
            _executed = true;
            _playerModel.SetLastPosition(_positionToLoadAfterComplete.position);
            _sceneChanger.GoToPuzzleScene(_puzzleSceneToLoad);
        }
    }
}