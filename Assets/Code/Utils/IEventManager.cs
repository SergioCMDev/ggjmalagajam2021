﻿using System;

namespace Signals.EventManager
{
    public interface IEventManager
    {
        void AddActionToSignal<T>(Action<T> actionToDo);
        void NotifySubscribers<T>(T signal);
        void RemoveActionFromSignal<T>(Action<T> changeLifeValue);
    }
}