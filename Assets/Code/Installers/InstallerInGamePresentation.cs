﻿using App.PlayerModelInfo;
using Domain.JsonTranslator;
using Domain.LoaderSaveGame;
using Domain.SaverGame;
using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

namespace Presentation.Installers
{
    public class InstallerInGamePresentation : MonoBehaviour
    {
        private GameObject _readInputPlayerPrefab,
            _languageManagerPrefab,
            _sceneChangerManagerPrefab, _audioManagerBase;

        private GameObject _readInputPlayerInstance,
            _languageManagerInstance,
            _sceneChangerManagerInstance, _audioManagerInstance;

        private void Awake()
        {
            //TODO USAR ADDRESABLES
            _readInputPlayerPrefab = Resources.Load("Prefabs/Managers/ReadInputPlayer") as GameObject;
            _languageManagerPrefab = Resources.Load("Prefabs/Managers/LanguageManager") as GameObject;
            _sceneChangerManagerPrefab = Resources.Load("Prefabs/Managers/SceneChangerManager") as GameObject;
            _audioManagerBase = Resources.Load("Prefabs/Managers/AudioManagerBase") as GameObject;
            
            ServiceLocator.Instance.RegisterModel<IPlayerModel>(new PlayerModel());
            ServiceLocator.Instance.RegisterModel<IAudioModel>(new AudioModel());
            ServiceLocator.Instance.RegisterService<IEventManager>(new EventManager());
            ServiceLocator.Instance.RegisterService<IJsonator>(new JsonUtililyTransformer());
            ServiceLocator.Instance.RegisterService<ISaver>(new SaveUsingPlayerPrefs());
            ServiceLocator.Instance.RegisterService<ILoader>(new LoadWithPlayerPrefs());
            
            var updatePlayerProgression = new UpdatePlayerProgression();
            updatePlayerProgression.Init();
            
            _readInputPlayerInstance = Instantiate(_readInputPlayerPrefab, transform);
            _languageManagerInstance = Instantiate(_languageManagerPrefab,transform);
            _sceneChangerManagerInstance = Instantiate(_sceneChangerManagerPrefab,transform);
            _audioManagerInstance = Instantiate(_audioManagerBase,transform);

            ServiceLocator.Instance.RegisterService(_languageManagerInstance
                .GetComponent<ILanguageManager>());
            ServiceLocator.Instance.RegisterService(_readInputPlayerInstance.GetComponent<ReadInputPlayer>());
            ServiceLocator.Instance.RegisterService(_sceneChangerManagerInstance.GetComponent<SceneChanger>());
            ServiceLocator.Instance.RegisterService(updatePlayerProgression);
            ServiceLocator.Instance.RegisterService(_audioManagerInstance.GetComponent<AudioManagerBase>());
            
            _languageManagerInstance
                .GetComponent<ILanguageManager>().SetActualLanguageText(LanguagesKeys.SPA);

            // DontDestroyOnLoad(this);
        }

        private void OnDestroy()
        {
            ServiceLocator.Instance.UnregisterService<ReadInputPlayer>();
            ServiceLocator.Instance.UnregisterService<IEventManager>();
            ServiceLocator.Instance.UnregisterService<ILanguageManager>();
        }
    }
}