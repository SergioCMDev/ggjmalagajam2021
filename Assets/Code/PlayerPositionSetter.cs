﻿using App.PlayerModelInfo;
using UnityEngine;
using Utils;

public class PlayerPositionSetter : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;
    private IPlayerModel _playerModel;

    private void Awake()
    {
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
    }
    void Start()
    {
        if (_playerModel.PlayerComesFromPuzzle()) {

            if (_playerModel.NumOfPuzzlesCompleted() < 4) {
                _playerTransform.transform.position = _playerModel.GetLastPosition();
                _playerModel.SetLastPosition(Vector3.zero);
                return;
            }

        }
    }

}
