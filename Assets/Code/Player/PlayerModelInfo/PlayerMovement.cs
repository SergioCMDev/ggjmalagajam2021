﻿using UnityEngine;
using Utils.Input;


public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _standingMoveSpeed;
    [SerializeField] private Animator _animator;
    [SerializeField] private Rigidbody _rigidbody;

    private ReadInputPlayer _readInputPlayer;
    private bool _canMove;
    private Vector3 _jumpMovement;
    private static readonly int MovementZ = Animator.StringToHash("MovementZ");
    private static readonly int MovementX = Animator.StringToHash("MovementX");
    private static readonly int Speed = Animator.StringToHash("Speed");

    private Vector3 _movementDirection;

    private Quaternion _rotation;


    private Transform _transform;

    void Start()
    {
        _transform = transform;
        _canMove = true;
        _readInputPlayer.OnPlayerPressControlButton += StopMovement;
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void StopMovement()
    {
        _canMove = !_canMove;
    }

    public void Init(ReadInputPlayer readInputPlayer)
    {
        _readInputPlayer = readInputPlayer;
    }

    void FixedUpdate()
    {
        if (!_canMove)
        {
            return;
        }

        // Debug.Log(_readInputPlayer.MovementInGameAxis);
        Vector3 input = _readInputPlayer.MovementInGameAxis;

        // _animator.SetFloat(MovementZ, input.z);
        // _animator.SetFloat(Speed, input.magnitude);


        Vector3 movement = Camera.main.transform.right * input.x + Camera.main.transform.forward * input.z;
        movement.y = 0;
        // _transform.S += movement * Time.deltaTime * _standingMoveSpeed;


        _rigidbody.MovePosition(_transform.position+movement * Time.deltaTime * _standingMoveSpeed);
    }

    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawRay(transform.position, _movementDirection);
    }
}