﻿using System.Collections.Generic;
using UnityEngine;

namespace App.PlayerModelInfo
{
    public class PlayerModel : IPlayerModel
    {
        private List<string> _passedPuzzles;
        private Vector3 _lastPositionToLoad;
        private bool _tutorialHouseStatus;

        public PlayerModel()
        {
            _passedPuzzles = new List<string>();
        }

        public void ResetData()
        {
        }

        public bool ContainsPuzzleData(string puzzleName)
        {
            return _passedPuzzles.Contains(puzzleName);
        }

        public void AddPassedPuzzle(string puzzleName)
        {
            if (ContainsPuzzleData(puzzleName)) return;
            _passedPuzzles.Add(puzzleName);
        }

        public void SetLastPosition(Vector3 positionToLoadAfterComplete)
        {
            _lastPositionToLoad = positionToLoadAfterComplete;
        }

        public Vector3 GetLastPosition()
        {
            return _lastPositionToLoad;
        }

        public void SetTutorialHouseStatus(bool status)
        {
            _tutorialHouseStatus = status;
        }

        public bool TutorialHouseShowed()
        {
            return _tutorialHouseStatus;
        }

        public bool PlayerComesFromPuzzle()
        {
            return GetLastPosition() != Vector3.zero;
        }

        public int NumOfPuzzlesCompleted() {
            return _passedPuzzles.Count;
        }

    }
}