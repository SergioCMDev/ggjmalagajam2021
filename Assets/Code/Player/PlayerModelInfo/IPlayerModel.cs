﻿using UnityEngine;

namespace App.PlayerModelInfo
{
    public interface IPlayerModel
    {
        void ResetData();
        bool ContainsPuzzleData(string puzzleName);
        void AddPassedPuzzle(string puzzleName);
        void SetLastPosition(Vector3 positionToLoadAfterComplete);
        Vector3 GetLastPosition();
        void SetTutorialHouseStatus(bool status);
        bool TutorialHouseShowed();
        bool PlayerComesFromPuzzle();
        int NumOfPuzzlesCompleted();
    }
}