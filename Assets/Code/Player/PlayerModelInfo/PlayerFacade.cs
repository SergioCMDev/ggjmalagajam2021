﻿using App.PlayerModelInfo;
using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

public class PlayerFacade : MonoBehaviour
{
    [SerializeField] private PlayerMovement _playerMovement;

    private IPlayerModel _playerModel;
    private IEventManager _eventManager;
    private ReadInputPlayer _readInputPlayer;
    // public IPlayerModel PlayerModel => _playerModel;

    void Awake()
    {
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _playerMovement.Init(_readInputPlayer);
    }
}