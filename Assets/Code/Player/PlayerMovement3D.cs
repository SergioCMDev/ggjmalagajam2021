﻿using UnityEngine;
using Utils.Input;


public class PlayerMovement3D : MonoBehaviour
{
    private ReadInputPlayer _readInputPlayer;

    [SerializeField] private float _standingMoveSpeed, _rotationSpeed, _maxDistanceToCheckObjectsWhenMove = 0.2f;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private Collider _collider;
    [SerializeField] private LayerMask _layerMaskToAvoid;

    // [SerializeField] private Rigidbody _rigidbody;
    // [SerializeField] private Animator _animator;
    // [SerializeField] private PlayerColliders playerColliders;
    private float _colliderOriginalSize;

    private bool _canMove;
    // private Vector3 _jumpMovement;
    // private static readonly int MovementZ = Animator.StringToHash("MovementZ");
    // private static readonly int MovementX = Animator.StringToHash("MovementX");
    // private static readonly int Crouch = Animator.StringToHash("Crouch");
    // private static readonly int Speed = Animator.StringToHash("Speed");

    private Vector3 _movementDirection;

    private Quaternion _rotation;
    // public Vector3 MovementDirection => _movementDirection;
    // public Quaternion GetRotation => _rotation;

    void Start()
    {
        Cursor.visible = false;
        _canMove = true;
        _collider = GetComponent<Collider>();
    }

    public void Init(ReadInputPlayer readInputPlayer)
    {
        _readInputPlayer = readInputPlayer;
    }


    void FixedUpdate()
    {
        if (!_canMove)
        {
            return;
        }

        Vector3 input = _readInputPlayer.MovementInGameAxis;
        _rotation = Quaternion.Euler(transform.rotation.eulerAngles + Vector3.up * input.x * _rotationSpeed);
        _rigidbody.MoveRotation(_rotation);


        Vector3 tempVect = transform.forward * input.z;
        tempVect = tempVect.normalized * _standingMoveSpeed * Time.fixedDeltaTime;
        Vector3 desieredPosition = transform.position + tempVect;
        Vector3 deltaMovement = desieredPosition - transform.position;

        Vector3 scale = Vector3.Scale(_collider.bounds.extents, deltaMovement.normalized);

        if (Physics.OverlapSphere(transform.position, _maxDistanceToCheckObjectsWhenMove, _layerMaskToAvoid).Length > 0)
        {
            Debug.Log("COli2");
            return;
        }
        if (Physics.Raycast(transform.position + scale, deltaMovement, _maxDistanceToCheckObjectsWhenMove, _layerMaskToAvoid))
        {
            Debug.Log("COli");
            return;
        }


        _rigidbody.MovePosition(transform.position + deltaMovement);
    }


    public void EnableMovement()
    {
        _canMove = true;
    }

    public void DisableMovement()
    {
        _canMove = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawRay(transform.position, _movementDirection);
    }
}