﻿// using System.Collections.Generic;
// using App.PlayerModelInfo;
// using Presentation.Items;
// using Signals.EventManager;
// using UnityEngine;
// using Utils;
//
// namespace Presentation.Player
// {
//     public class ItemInteractor : MonoBehaviour
//     {
//         [SerializeField] private LayerMask _layersToAllowInteractions;
//         [SerializeField] private float _sphereRadius;
//
//         private InteractableBase _interactableItem;
//         private float _xRotation, _yRotation, _xMouseValue, _yMouseValue;
//
//         private IPlayerModel _playerModel;
//         private bool _stopInteracting = false;
//
//         private IEventManager _eventManager;
//         private Collider[] _itemsNearPlayer;
//         private List<GameObject> _itemsToInteract;
//
//         private void Awake()
//         {
//             _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
//         }
//
//         private void Start()
//         {
//             // _eventManager.AddActionToSignal<ShowPictureOnCanvasSignal>(HideTexts);
//             // _eventManager.AddActionToSignal<HidePictureOnCanvasSignal>(ShowTexts);
//             _itemsNearPlayer = new Collider[5];
//             _itemsToInteract = new List<GameObject>();
//         }
//
//         // private void ShowTexts(HidePictureOnCanvasSignal obj)
//         // {
//         //     _stopInteracting = false;
//         // }
//         //
//         // private void HideTexts(ShowPictureOnCanvasSignal obj)
//         // {
//         //     _stopInteracting = true;
//         // }
//
//         public void Init(IPlayerModel playerModel)
//         {
//             _playerModel = playerModel;
//         }
//
//         private void Update()
//         {
//             _itemsNearPlayer = Physics.OverlapSphere(transform.position, _sphereRadius, _layersToAllowInteractions);
//
//             if (_itemsNearPlayer.Length > 0)
//             {
//                 foreach (var item in _itemsNearPlayer)
//                 {
//                     if (ItemIsInFrontOfPlayer(item) && _itemsToInteract.Contains(item.gameObject))
//                     {
//                         _itemsToInteract.Add(item.gameObject);
//                     }
//                 }
//
//                 Debug.Log(_itemsNearPlayer[0].gameObject.name);
//             }
//
//             // var itemInFrontOfPlayer = raycastHit.collider.GetComponent<InteractableBase>();
//             // if (!itemInFrontOfPlayer || !itemInFrontOfPlayer.enabled || _stopInteracting) return;
//             // _interactableItem = itemInFrontOfPlayer;
//
//
//             // _interactableItem.ShowMessageOverItem();
//         }
//
//         private bool ItemIsInFrontOfPlayer(Collider item)
//         {
//             bool areInOppositeDirection =
//                 Vector3Utils.VectorsAreInOppositeDirection(transform.forward, item.gameObject.transform.position);
//  
//             Debug.Log(areInOppositeDirection ? "Opposite" : " NOT Opposite");
//
//             return areInOppositeDirection;
//         }
//
//
//         public void InteractWithItem()
//         {
//             if (!_interactableItem)
//             {
//                 return;
//             }
//         }
//
//
//         private void OnDrawGizmosSelected()
//         {
//             Gizmos.color = Color.red;
//             //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
//             Gizmos.DrawWireSphere(transform.position, _sphereRadius);
//         }
//
//         // public void ResetItemToPick()
//         // {
//         //     if (!_interactableItem) return;
//         //     _interactableItem.HideMessageOverItem();
//         //     _interactableItem = null;
//         // }
//     }
// }