﻿using UnityEngine;

public struct ObjectInfo
{
    public Vector3 offset;
    public Transform gameObject;
}