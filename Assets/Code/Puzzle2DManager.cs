﻿using Signals.EventManager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class Puzzle2DManager : MonoBehaviour
{
    [SerializeField] private GameObject _imgFinalCont;
    [SerializeField] private Sprite _imgResuelto;
    [SerializeField] private float _timeToWaitUntilWinSignal;
    [SerializeField] private PiezaHandler[] _listPiezas;
    [SerializeField] private GameObject _imgFinalCentral;
    private int _piezasSinColocar;

    private Timer _timer;

    // Start is called before the first frame update
    void Start()
    {
        _piezasSinColocar = 9;
        _timer = new Timer();
        _timer.OnTimerEnds += ThrowWinSignal;
        _timer.SetTimeToWait(_timeToWaitUntilWinSignal);
        foreach (PiezaHandler pieza in _listPiezas)
        {
            pieza.PiezaOnSite += PiezaColocada;
        }
    }

    private void PiezaColocada()
    {
        _piezasSinColocar--;
        if (_piezasSinColocar <= 0)
        {
            //Si el puzzle se resuelve, cambiar el Sprite del componente SpriteRenderer a la imagen en color.
            _imgFinalCont.GetComponent<SpriteRenderer>().sprite = _imgResuelto;
            _imgFinalCentral.SetActive(true);
            Invoke(nameof(ThrowWinSignal), _timeToWaitUntilWinSignal);
            // StartCoroutine(_timer.TimerCoroutine());
            //Timer para dejar ver el puzzle en color antes de pasar de escena
        }
    }

    private void ThrowWinSignal()
    {
        // _timer.OnTimerEnds -= ThrowWinSignal;

        new PlayerHasCompletedPuzzleSignal().Execute();
    }
}