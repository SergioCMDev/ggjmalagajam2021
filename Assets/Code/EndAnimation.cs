﻿using Cinemachine;
using Signals.EventManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class EndAnimation : MonoBehaviour
{

    [SerializeField] private Transform _playerLocationToEndAnimation;
    [SerializeField] private Animator _animatorTapa, _animatorGato;
    [SerializeField] private GameObject _vCamObjUser, _vCamObjEndAnim;
    
    private Animator _animatorCamera;

    private IEventManager _eventManager;

    private void Awake() {

        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();

    }

    // Start is called before the first frame update
    void Start()
    {

        _eventManager.AddActionToSignal<EndAnimationCatCompletedSignal>(StartEndAnimationCamera);

        _animatorCamera = _vCamObjEndAnim.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartEndAnimations() {

        _animatorTapa.SetTrigger("EndAnimationTrigger");
        _animatorGato.SetTrigger("EndAnimationTrigger");

    }

    private void StartEndAnimationCamera(EndAnimationCatCompletedSignal obj) {

        _vCamObjEndAnim.SetActive(true);
        _vCamObjEndAnim.transform.position = _playerLocationToEndAnimation.position;
        //_animatorCamera.transform.position = _playerLocationToEndAnimation.position;
        //_animatorPlayerCamera.transform.rotation = new Quaternion(0, 180, 0, 0);
        //var vcamBody = _animatorPlayerCamera.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent(CinemachineCore.Stage.Body);
        _vCamObjUser.SetActive(false);
        _animatorCamera.SetTrigger("EndAnimationTrigger");

    }

}
