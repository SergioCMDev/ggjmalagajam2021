﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScenesListModel : MonoBehaviour
{
    [SerializeField] private List<SceneInfo> _sceneInfos;

    public SceneInfo SceneInfoByName(string sceneName)
    {
        return GetSceneByName(sceneName);
    }

    public SceneInfo GetSceneById(int id)
    {
        var sceneInfo = _sceneInfos.SingleOrDefault(x => x.SceneId == id);
        if (string.IsNullOrEmpty(sceneInfo.SceneName))
        {
            throw new Exception($"Scene Info Of {sceneInfo.SceneName} is empty");
        }

        return sceneInfo;
    }

    private SceneInfo GetSceneByName(string sceneName)
    {
        var sceneInfo = _sceneInfos.SingleOrDefault(x => x.SceneName == sceneName);
        if (sceneInfo.SceneId < 0)
        {
            throw new Exception($"Scene Info Of {sceneName} is < 0");
        }

        return sceneInfo;
    }

    public SceneInfo GetNextScene(string currentSceneName)
    {
        int idCurrentScene = SceneInfoByName(currentSceneName).SceneId;
        if (idCurrentScene < 0)
        {
            throw new Exception($"Id of Scene is < 0");
        }

        if (idCurrentScene + 1 < _sceneInfos.Count)
        {
            idCurrentScene++;
        }
        else
        {
            return new SceneInfo {SceneName = "Credits"};
        }

        return GetSceneById(idCurrentScene);
    }

    public int GetSceneCount()
    {
        return _sceneInfos.Count;
    }
}