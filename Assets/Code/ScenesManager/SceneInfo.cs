﻿using System;

[Serializable]
public struct SceneInfo
{
    public string SceneName;
    public int SceneId;
}