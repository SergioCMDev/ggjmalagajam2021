﻿using System;
using DG.Tweening;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

public class SceneChanger : MonoBehaviour
{
    private IEventManager _eventManager;
    private Sequence _sequence;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void HandleCompletedLevel(PlayerHasCompletedLevelSignal obj)
    {
        new SavePlayerProgressSignal().Execute();
    }


    public void RestartScene()
    {
        SceneManager.LoadScene(GetCurrentScene());
    }


    public void GoToInitScene()
    {
        SceneManager.LoadScene("InitScene 1");
    }

    public String GetCurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }


    public void GoToHouseScene()
    {
        SceneManager.LoadScene("Casa");
    }
    
    public void GoToPuzzleScene(string sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void GoToCreditsScene()
    {
        SceneManager.LoadScene("Creditos");
    }
}