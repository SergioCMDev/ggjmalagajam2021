﻿using UnityEngine;

public interface ISelectable
{
    void Select();
    void Deselect();
    void SetData(Vector3 objOffset);
}