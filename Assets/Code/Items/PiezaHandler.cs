﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using Utils;
using Utils.Input;

public class PiezaHandler : MonoBehaviour
{
    [SerializeField] private Collider2D _correctPosTest;
    [SerializeField] private LayerMask _layersToAvoidInteractions;
    [SerializeField] private LayerMask _layersToAllowInteractions;
    private ReadInputPlayer _readInputPlayer;
    private GameObject _beingDragged;
    private bool _interactable, _itsCorrectPos;
    private Collider2D _collider2D, _circleCollider2D, _collider2DTemp;
    private Vector2 _screenPoint;
    private Collider2D[] _overlappointResults, _collidersResults;
    private ContactFilter2D _contactFilter2D;

    private Camera _camera;
    public event Action PiezaOnSite;


    private void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _interactable = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;

        _readInputPlayer.OnPlayerPressLeftButtonMouse += LeftClickPressed;
        _readInputPlayer.OnPlayerReleaseLeftButtonMouse += LeftClickReleased;
        _collider2D = GetComponent<BoxCollider2D>();
        _circleCollider2D = GetComponent<CircleCollider2D>();
        _beingDragged = null;
        _contactFilter2D.NoFilter();
        //_contactFilter2D = new ContactFilter2D();
        _overlappointResults = new Collider2D[15];
        _collidersResults = new Collider2D[15];
    }

    /*private void Start() {
        throw new NotImplementedException();
    }*/

    // Update is called once per frame
    void Update()
    {
        _screenPoint = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());

        /*if (_movementActive) {

            _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            //Debug.Log("ScreenPoint " + _screenPoint);
            _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y, _screenPoint.z));

            Vector3 curPosition = Camera.main.ScreenToWorldPoint(_screenPoint) - _offset;
            transform.position = curPosition;
        }*/
    }

    private void FixedUpdate()
    {
        if (_beingDragged)
        {
            transform.position = new Vector3(_screenPoint.x, _screenPoint.y, _beingDragged.transform.position.z);
        }
    }

    /*private void OnTriggerEnter2D(Collider2D collision) {

        if (collision.gameObject.GetComponent<Collider2D>() == _correctPosTest) {

            _itsCorrectPos = true;

        }

    }

    private void OnTriggerExit2D(Collider2D collision) {
        


    }*/
    private void OnMouseDown()
    {
        Debug.Log("f");
    }

    private void LeftClickPressed()
    {
        /*if (_interactable) {

            _overlappointResults = Physics2D.OverlapPointAll(_screenPoint);
            foreach (Collider2D collider2D in _overlappointResults) {

                if (collider2D.gameObject == gameObject) {
                    _beingDragged = gameObject;
                    break;
                }

            }

        }*/

        if (_interactable)
        {
            _collider2DTemp = Physics2D.OverlapPoint(_screenPoint, _layersToAllowInteractions);

            //TODO
            // Ray ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
            // RaycastHit2D hit = Physics2D.Raycast(_camera.transform.position, _camera.transform.position - ray.direction,
            //     _layersToAllowInteractions);
            // RaycastHit2D hit2 = Physics2D.Raycast(_camera.transform.position, ray.origin, _layersToAllowInteractions);
            // RaycastHit2D hit3 = Physics2D.Raycast(ray.origin, ray.origin - _camera.transform.position,
            //     1000,_layersToAllowInteractions );
            // if (hit3.collider)
            // {
            //     Debug.Log(hit3.collider);
            // }

            if (_collider2DTemp && _collider2DTemp.gameObject == gameObject)
            {
                _beingDragged = gameObject;
                gameObject.GetComponent<SpriteRenderer>().sortingOrder += 1;
            }
        }


        /*if (_interactable && Physics2D.OverlapPointAll(_screenPoint).gameObject == gameObject) {
            _beingDragged = true;
        }*/
    }

    private void LeftClickReleased()
    {
        if (_beingDragged)
        {
            //_beingDragged.GetComponent<SpriteRenderer>().sortingOrder = 1;
            _beingDragged = null;

            if (Physics2D.OverlapCollider(_collider2D, _contactFilter2D, _collidersResults) > 0)
            {
                _itsCorrectPos = false;

                foreach (Collider2D collider2D in _collidersResults)
                {
                    //Debug.Log("Itera");
                    if (collider2D == _correctPosTest)
                    {
                        _itsCorrectPos = true;
                        transform.position = _correctPosTest.transform.position;
                        _interactable = false;
                        gameObject.layer = _layersToAvoidInteractions;
                        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
                        PiezaOnSite.Invoke();
                        Debug.Log("Detecta " + gameObject.name);
                        break;
                    }
                }
            }
        }
    }
}