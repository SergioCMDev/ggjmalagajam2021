﻿using UnityEngine;
using Utils;

namespace Presentation.Items
{
    public class Cat : InteractableItem
    {
        [SerializeField] private string _keyTextToSay;
        [SerializeField] private Sprite _spriteToShow, _catName;
        private ILanguageManager _languageManager;
        private string _textToSay;
        private bool _interacting;

        private void Awake()
        {
            _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        }

        private void Start()
        {
            base.Start();
            _interacting = false;
            _textToSay = _languageManager.GetCatTextByKey(_keyTextToSay);
        }

        public override void InteractWithObject()
        {
            base.InteractWithObject();
            _interacting = true;
            new ShowCatChatBoxSignal()
            {
                Sprite = _spriteToShow,
                Text = _textToSay,
                SpriteCatName = _catName
            }.Execute();
        }

        public override void OverObject()
        {
            base.OverObject();
            Debug.Log("CAT");
        }

        public override void StopOverObject()
        {
            base.StopOverObject();
            if (!_interacting) return;
            new HideCatChatBoxSignal().Execute();
            Debug.Log(" STOP CAT");
        }
    }
}