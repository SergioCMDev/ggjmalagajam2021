﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.PlayerModelInfo;
using Presentation.Items;
using UnityEngine;
using Utils;

public class PuzzleLoader : InteractableItem
{
    [SerializeField] private string _puzzleSceneToLoad;
    [SerializeField] private Transform _positionToLoadAfterComplete;
    private SceneChanger _sceneChanger;
    private bool _interacting;
    private IPlayerModel _playerModel;

    private void Awake()
    {
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
    }

    void Start()
    {
        base.Start();
        _interacting = false;
    }


    public override void OverObject()
    {
        if (PassedPuzzle()) {
            return;
        }
        base.OverObject();
        Debug.Log("FRIGDE");
    }

    public override void InteractWithObject()
    {
        if (PassedPuzzle())
        {
            return;
        }

        base.InteractWithObject();
        
        _playerModel.SetLastPosition(_positionToLoadAfterComplete.position);
        _sceneChanger.GoToPuzzleScene(_puzzleSceneToLoad);
        _interacting = true;
    }

    private bool PassedPuzzle()
    {
        return _playerModel.ContainsPuzzleData(_puzzleSceneToLoad);
    }

    public override void StopOverObject()
    {
        base.StopOverObject();
        Debug.Log("FRIGDE NO");
    }
}