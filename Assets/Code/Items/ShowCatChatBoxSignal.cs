﻿using Signals;
using UnityEngine;

namespace Presentation.Items
{
    public class ShowCatChatBoxSignal : SignalBase
    {
        public Sprite Sprite;
        public string Text;
        public Sprite SpriteCatName;
    }
}