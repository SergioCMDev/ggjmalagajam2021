﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;
using Utils.Input;

public enum Orientation
{
    HORIZONTAL,
    VERTICAL
}

public class CubeSlider : MonoBehaviour, ISelectable
{
    [SerializeField] private Orientation _orientation;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float _moveSpeed, _maxDeltaToMove = 0.3f, _maxDistanceToCheckObjectsWhenMove = 0.2f;

    private Vector3 _screenPoint, _offsetFromCamera, _offsetFromPlane, _desiredPos;
    private bool _canMove, _isSelected;
    private ReadInputPlayer _readInputPlayer;
    private Plane _plane;
    private Collider _collider;

    private void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
    }

    void Start()
    {
        _canMove = false;
        _readInputPlayer.OnPlayerPressRightButtonMouse += ChangeOrientation;
        _plane = new Plane(transform.up, transform.position);
        _collider = GetComponent<Collider>();
    }

    private void OnDestroy()
    {
        _readInputPlayer.OnPlayerPressRightButtonMouse -= ChangeOrientation;
    }

    private void ChangeOrientation()
    {
        if (_isSelected)
        {
            _orientation = _orientation == Orientation.HORIZONTAL ? Orientation.VERTICAL : Orientation.HORIZONTAL;
        }
    }

    private void Update()
    {
        if (!_isSelected) return;
        _canMove = Mouse.current.delta.ReadValue().sqrMagnitude > 0.1f;
    }

    void FixedUpdate()
    {
        if (!_canMove || !_isSelected) return;
        Ray ray = Camera.main.ScreenPointToRay(
            new Vector3(Mouse.current.position.ReadValue().x, Mouse.current.position.ReadValue().y, 0));

        _plane.Raycast(ray, out var distance);
        Vector3 finalPoint = ray.direction * distance + ray.origin;
        _offsetFromPlane = Vector3.ProjectOnPlane(_offsetFromCamera, _plane.normal);
        finalPoint -= _offsetFromPlane;

        _desiredPos = _orientation == Orientation.VERTICAL
            ? new Vector3(finalPoint.x, transform.position.y, transform.position.z)
            : new Vector3(transform.position.x, transform.position.y, finalPoint.z);

        Vector3 deltaMovement = _desiredPos - transform.position;
        deltaMovement = Vector3.ClampMagnitude(deltaMovement, _maxDeltaToMove);
        Vector3 scale = Vector3.Scale(_collider.bounds.extents, deltaMovement.normalized);

        if (Physics.Raycast(transform.position + scale, deltaMovement, _maxDistanceToCheckObjectsWhenMove)) return;

        _rigidbody.MovePosition(deltaMovement + transform.position);
    }


    private void OnCollisionEnter(Collision other)
    {
        
        Debug.Log("COllision");
        _rigidbody.MovePosition(transform.position);

        _canMove = false;
        Deselect();
    }

    public void Select()
    {
        _isSelected = true;
    }

    public void Deselect()
    {
        _isSelected = false;
        _offsetFromCamera = new Vector3();
    }

    public void SetData(Vector3 objOffset)
    {
        _offsetFromCamera = objOffset;
    }
}