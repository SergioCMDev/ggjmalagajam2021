﻿using System;
using UnityEngine;

namespace Presentation.Items
{
    public  class InteractableItem : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;
        private Outline _outline;

        protected void Start()
        {
            _outline = GetComponent<Outline>();
        }

        public virtual void InteractWithObject()
        {
            Debug.Log("Play sound");
            PlaySound(_audioSource);
        }


        private void PlaySound(AudioSource audioSource)
        {
            audioSource.Play();
        }

        public virtual void OverObject()
        {
            _outline.enabled = true;
        }

        public virtual void StopOverObject()
        {
            _outline.enabled = false;
        }
    }
}