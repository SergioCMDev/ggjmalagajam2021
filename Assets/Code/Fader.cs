﻿using System;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class Fader : MonoBehaviour
{
    private IEventManager _eventManager;
    [SerializeField] private Image _image;
    [SerializeField] private float _timerToFade;
    private SwitcherFade _switcherFade;
    public event Action OnChangeSceneToCredits = delegate { };

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _eventManager.AddActionToSignal<StartFadeSignal>(StartFade);
        _switcherFade = new SwitcherFade(_image, _timerToFade, true);
    }

    private void Start()
    {
    }


    private void StartFade(StartFadeSignal obj)
    {
        _switcherFade.OnCoroutineEnd += EndGame;

        StartCoroutine(_switcherFade.Start());
    }

    private void EndGame(SwitcherFade obj)
    {
        _switcherFade.OnCoroutineEnd -= EndGame;
        OnChangeSceneToCredits.Invoke();
    }
}