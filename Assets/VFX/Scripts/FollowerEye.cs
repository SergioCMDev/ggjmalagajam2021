﻿using UnityEngine;

namespace VFX
{
    public class FollowerEye : MonoBehaviour
    {
        [SerializeField] private Transform player = default;
        [SerializeField] private float maxRadius = 1f;

        private Vector3 initPosition;
        private Vector2 xzInitPosition;

        private void Start()
        {
            initPosition = transform.position;
            xzInitPosition = new Vector2(initPosition.x, initPosition.z);
        }

        private void Update()
        {
            Vector2 xzPlayerPosition = new Vector2(player.position.x, player.position.z);
            Vector2 playerDirection = (xzPlayerPosition - xzInitPosition).normalized;

            transform.position = initPosition + (new Vector3(playerDirection.x, 0, playerDirection.y) * maxRadius);
        }
    }
}
