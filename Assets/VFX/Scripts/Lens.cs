﻿using UnityEngine;

namespace VFX
{
    public class Lens : MonoBehaviour
    {
        [SerializeField] private Transform mainCamera;

        private void Update()
        {
            Quaternion targetRotation = Quaternion.LookRotation((transform.position - mainCamera.position).normalized, Vector3.up);

            transform.rotation = targetRotation * Quaternion.Euler(90f, 0, 0f);
        }
    }
}
